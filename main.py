from fastapi import FastAPI
from pydantic import BaseModel
from transformers import AutoTokenizer, TFAutoModelForSeq2SeqLM

class Message(BaseModel):
    text: str

model = TFAutoModelForSeq2SeqLM.from_pretrained("./model")
tokenizer = AutoTokenizer.from_pretrained("facebook/blenderbot_small-90M")

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello World!"}

@app.post("/talk")
async def talk_to_me(data: Message):
    input_text = data.text
    encoded_query = tokenizer(input_text, return_tensors="tf", padding="max_length", truncation=True)
    input_ids = encoded_query["input_ids"]
    attention_mask = encoded_query["attention_mask"]
    answer = model.generate(input_ids, attention_mask=attention_mask)
    decoded_answer = tokenizer.decode(answer.numpy()[0])
    return {"text": decoded_answer}